import { environment } from '../../environments/environment';

class Clients {
  static readonly base = `${environment.serverUrl}/clients`;
  static readonly create = Clients.base;
  static readonly getAll = Clients.base;
  static readonly getLookup = `${Clients.base}/lookup`;
  static readonly checkEmailAvailability = (email: string) => `${Clients.base}/check/unique/by-email?email=${email}`;
  static readonly findById = (id: number) => `${Clients.base}/${id}`;
  static readonly update = (id: number) => `${Clients.base}/${id}`;
}

class Cars {
  static readonly base = `${environment.serverUrl}/cars`;
  static readonly create = Cars.base;
  static readonly getAll = Cars.base;
  static readonly getLookup = `${Cars.base}/lookup`;
  static readonly findById = (id: number) => `${Cars.base}/${id}`;
  static readonly delete = (id: number) => `${Cars.base}/${id}`;

}

class Complexes {
  static readonly base = `${environment.serverUrl}/insurance-complexes`;
  static readonly getAll = Complexes.base;
  static readonly getLookup = `${Complexes.base}/lookup`;
  static readonly findById = (id: number) => `${Complexes.base}/${id}`;
}

class Contracts {
  static readonly base = `${environment.serverUrl}/insurance-contracts`;
  static readonly create = Contracts.base;
  static readonly getAll = Contracts.base;
  static readonly findById = (id: number) => `${Contracts.base}/${id}`;
}

export class Endpoints {
  static readonly CLIENTS = Clients;
  static readonly CARS = Cars;
  static readonly COMPLEXES = Complexes;
  static CONTRACTS = Contracts;
}
