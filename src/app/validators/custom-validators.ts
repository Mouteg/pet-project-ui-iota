import { FormControl } from '@angular/forms';

export class CustomValidators {

  static past(form: FormControl): object {
    const now = Date.now();
    if (form.value > now) {
      return ({ past: 'Should be date in the past' });
    }
    return null;
  }

  static digit(form: FormControl): object {
    if (Number(form.value) || !form.value) {
      return null;
    }
    return ({ digit: 'Should be digit' });
  }
}
