import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeLayoutComponent } from './modules/layouts/home-layout/home-layout.component';
import { ClientListLayoutComponent } from './modules/layouts/client-list-layout/client-list-layout.component';
import { CarListLayoutComponent } from './modules/layouts/car-list-layout/car-list-layout.component';
import { ContractListLayoutComponent } from './modules/layouts/contract-list-layout/contract-list-layout.component';
import { ComplexListLayoutComponent } from './modules/layouts/complex-list-layout/complex-list-layout.component';

const routes: Routes = [
  { path: '', component: HomeLayoutComponent },
  { path: 'clients', component: ClientListLayoutComponent },
  { path: 'cars', component: CarListLayoutComponent },
  { path: 'insurance-contracts', component: ContractListLayoutComponent },
  { path: 'insurance-complexes', component: ComplexListLayoutComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
