import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Endpoints } from '../constants/endpoints';
import { InsuranceContractCreateDto, InsuranceContractListDto } from '../models/insurance-contract.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContractService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<InsuranceContractListDto[]> {
    return this.http.get<InsuranceContractListDto[]>(Endpoints.CONTRACTS.getAll);
  }

  create(createDto: InsuranceContractCreateDto): Observable<InsuranceContractListDto> {
    return this.http.post<InsuranceContractListDto>(Endpoints.CONTRACTS.create, createDto);
  }
}
