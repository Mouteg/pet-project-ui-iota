import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Endpoints } from '../constants/endpoints';
import { HttpClient } from '@angular/common/http';
import { InsuranceComplex } from '../models/insurance-complex.model';
import { LookupDto } from '../models/base.model';

@Injectable({
  providedIn: 'root'
})
export class ComplexService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<InsuranceComplex[]> {
    return this.http.get<InsuranceComplex[]>(Endpoints.COMPLEXES.getAll);
  }

  getLookup(): Observable<LookupDto[]> {
    return this.http.get<LookupDto[]>(Endpoints.COMPLEXES.getLookup);
  }
}
