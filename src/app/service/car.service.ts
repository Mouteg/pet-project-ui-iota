import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Car, CarCreateDto, CarListDto } from '../models/car.model';
import { Endpoints } from '../constants/endpoints';
import { LookupDto } from '../models/base.model';

@Injectable({
  providedIn: 'root'
})
export class CarService {

  constructor(private http: HttpClient) {
  }

  getAll(): Observable<CarListDto[]> {
    return this.http.get<CarListDto[]>(Endpoints.CARS.getAll);
  }

  create(car: CarCreateDto): Observable<CarListDto> {
    return this.http.post<CarListDto>(Endpoints.CARS.create, car);
  }

  getById(id: number): Observable<Car> {
    return this.http.get<Car>(Endpoints.CARS.findById(id));
  }

  getLookup(): Observable<LookupDto[]> {
    return this.http.get<LookupDto[]>(Endpoints.CARS.getLookup);
  }

  delete(id: number): Observable<any> {
    console.log('123');
    return this.http.delete(Endpoints.CARS.delete(id));
  }
}
