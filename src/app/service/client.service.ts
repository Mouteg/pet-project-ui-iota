import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Client, ClientCreateDto, ClientEditDto } from '../models/client.model';
import { Endpoints } from '../constants/endpoints';
import { LookupDto } from '../models/base.model';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) {
  }

  getById(id: number): Observable<Client> {
    return this.http.get<Client>(Endpoints.CLIENTS.findById(id));
  }

  getAll(): Observable<Client[]> {
    return this.http.get<Client[]>(Endpoints.CLIENTS.getAll);
  }

  update(client: ClientEditDto): Observable<Client> {
    return this.http.put<Client>(Endpoints.CLIENTS.update(client.id), client);
  }
// TODO
  create(createDto: ClientCreateDto): Observable<Client> {
    return this.http.post<Client>(Endpoints.CLIENTS.create, createDto);
  }

  checkEmailAvailability(email: string): Observable<boolean> {
    return this.http.get<boolean>(Endpoints.CLIENTS.checkEmailAvailability(email));
  }

  getLookup(): Observable<LookupDto[]> {
    return this.http.get<LookupDto[]>(Endpoints.CLIENTS.getLookup);
  }
}
