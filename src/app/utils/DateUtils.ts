import { DateTime } from '../models/types.model';

export class DateUtils {

  static formatToSimplifiedDate(epoch: DateTime): string {
    const date = new Date(epoch);
    return date.getFullYear() + '/' + date.getMonth() + '/' + date.getDay();
  }
}

