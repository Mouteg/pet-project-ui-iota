import { BaseModel } from './base.model';
import { DateTime } from './types.model';

export class Car implements BaseModel, CarListDto {
  id: number;
  uuid: number;
  createdAt: DateTime;
  updatedAt: DateTime;

  color: Color;
  engineCapacity: number;
  weight: number;
  yearOfManufacture: number;
}

export enum Color {
  RED = 'RED',
  GRAY = 'GRAY',
  BLUE = 'BLUE',
  PURPLE = 'PURPLE',
  WHITE = 'WHITE',
  BLACK = 'BLACK',
  CYAN = 'CYAN',
  ORANGE = 'ORANGE',
  LIME = 'LIME',
  AQUA = 'AQUA'
}

export interface CarCreateDto {
  color: Color;
  engineCapacity: number;
  weight: number;
  yearOfManufacture: number;
  clientId: number;
}

export interface CarListDto {
  id: number;
  color: Color;
  engineCapacity: number;
  weight: number;
  yearOfManufacture: number;
}
