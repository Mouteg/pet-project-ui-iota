import { BaseModel } from './base.model';
import { DateTime } from './types.model';

export class InsuranceContract implements BaseModel {
  id: number;
  uuid: number;
  createdAt: DateTime;
  updatedAt: DateTime;

  carId: number;
  clientId: number;
  insuranceComplexes: Array<number>;
}

export interface InsuranceContractCreateDto {
  clientId: number;
  carId: number;
  insuranceComplexesIds: Array<number>;
}

export interface InsuranceContractListDto {
  clientId: number;
  carId: number;
  insuranceComplexesIds: Array<number>;
}
