export class InsuranceComplex {
  id: number;
  duration: number;
  compensationPercent: number;
  damageLevel: DamageLevel;
  coveredPart: CoveredPart;
}

export enum DamageLevel {
  LOW = 'LOW',
  MEDIUM = 'MEDIUM',
  HIGH = 'HIGH',
}

export enum CoveredPart {
  WHEELS = 'WHEELS',
  HULL = 'HULL',
  WINDSCREEN = 'WINDSCREEN',
  DOORS = 'DOORS'
}
