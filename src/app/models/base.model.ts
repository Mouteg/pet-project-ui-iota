import { DateTime } from './types.model';

export interface BaseModel {
  id: number;
  uuid: number;
  createdAt: DateTime;
  updatedAt: DateTime;
}

export interface LookupDto {
  id: number;
  label: string;
}
