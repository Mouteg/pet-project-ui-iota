import { InsuranceContract } from './insurance-contract.model';
import { DateTime } from './types.model';
import { BaseModel } from './base.model';

export class Client implements BaseModel, ClientListDto, ClientEditDto {
  id: number;
  uuid: number;
  createdAt: DateTime;
  updatedAt: DateTime;

  email: string;
  firstName: string;
  lastName: string;
  birthDate: DateTime;
  insuranceContracts: InsuranceContract;
  cars: Array<number>;
}

export interface ClientCreateDto {
  email: string;
  firstName: string;
  lastName: string;
  password: string;
  birthDate: DateTime;
}

export interface ClientListDto {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  birthDate: DateTime;
}

export interface ClientEditDto {
  id: number;
  firstName: string;
  lastName: string;
  birthDate: DateTime;
}
