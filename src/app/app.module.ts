import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { NavigationBarComponent } from './modules/components/navigation-bar/navigation-bar.component';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { RouterModule } from '@angular/router';
import { HomeLayoutComponent } from './modules/layouts/home-layout/home-layout.component';
import { ClientTableComponent } from './modules/components/client/client-table/client-table.component';
import { MatTableModule } from '@angular/material/table';
import { HttpClientModule } from '@angular/common/http';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatInputModule } from '@angular/material/input';
import { ClientCreateDialogComponent } from './modules/components/client/client-create-dialog/client-create-dialog.component';
import { ClientEditDialogComponent } from './modules/components/client/client-edit-dialog/client-edit-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { ClientCreateButtonComponent } from './modules/components/client/client-create-button/client-create-button.component';
import { ClientEditButtonComponent } from './modules/components/client/client-edit-button/client-edit-button.component';
import { ClientInfoDialogComponent } from './modules/components/client/client-info-dialog/client-info-dialog.component';
import { ClientInfoButtonComponent } from './modules/components/client/client-info-button/client-info-button.component';
import { ClientListLayoutComponent } from './modules/layouts/client-list-layout/client-list-layout.component';
import { CarListLayoutComponent } from './modules/layouts/car-list-layout/car-list-layout.component';
import { ContractListLayoutComponent } from './modules/layouts/contract-list-layout/contract-list-layout.component';
import { ComplexListLayoutComponent } from './modules/layouts/complex-list-layout/complex-list-layout.component';
import { CarTableComponent } from './modules/components/car/car-table/car-table.component';
import { CarCreateButtonComponent } from './modules/components/car/car-create-button/car-create-button.component';
import { CarCreateDialogComponent } from './modules/components/car/car-create-dialog/car-create-dialog.component';
import { CarInfoButtonComponent } from './modules/components/car/car-info-button/car-info-button.component';
import { CarInfoDialogComponent } from './modules/components/car/car-info-dialog/car-info-dialog.component';
import { MatSelectModule } from '@angular/material/select';
import { ContractCreateDialogComponent } from './modules/components/contract/contract-create-dialog/contract-create-dialog.component';
import { ContractCreateButtonComponent } from './modules/components/contract/contract-create-button/contract-create-button.component';
import { ContractTableComponent } from './modules/components/contract/contract-table/contract-table.component';
import { ComplexTableComponent } from './modules/components/complex/complex-table/complex-table.component';
import { CarDeleteButtonComponent } from './modules/components/car/car-delete-button/car-delete-button.component';
import { CarDeleteDialogComponent } from './modules/components/car/car-delete-dialog/car-delete-dialog.component';


@NgModule({
  declarations: [
    AppComponent,
    NavigationBarComponent,
    HomeLayoutComponent,
    ClientTableComponent,
    ClientCreateDialogComponent,
    ClientEditDialogComponent,
    ClientCreateButtonComponent,
    ClientEditButtonComponent,
    ClientInfoDialogComponent,
    ClientInfoButtonComponent,
    ClientListLayoutComponent,
    CarListLayoutComponent,
    ContractListLayoutComponent,
    ComplexListLayoutComponent,
    CarTableComponent,
    CarCreateButtonComponent,
    CarCreateDialogComponent,
    CarInfoButtonComponent,
    CarInfoDialogComponent,
    ContractCreateDialogComponent,
    ContractCreateButtonComponent,
    ContractTableComponent,
    ComplexTableComponent,
    CarDeleteButtonComponent,
    CarDeleteDialogComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatButtonToggleModule,
    MatTableModule,
    HttpClientModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatInputModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
