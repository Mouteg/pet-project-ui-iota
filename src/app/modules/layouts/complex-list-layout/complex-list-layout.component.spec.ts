import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComplexListLayoutComponent } from './complex-list-layout.component';

describe('ComplexListLayoutComponent', () => {
  let component: ComplexListLayoutComponent;
  let fixture: ComponentFixture<ComplexListLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComplexListLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplexListLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
