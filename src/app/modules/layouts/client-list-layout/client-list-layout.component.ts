import { Component, ViewChild } from '@angular/core';
import { ClientTableComponent } from '../../components/client/client-table/client-table.component';
import { ClientListDto } from '../../../models/client.model';

@Component({
  selector: 'app-client-list-layout',
  templateUrl: './client-list-layout.component.html',
  styleUrls: ['./client-list-layout.component.scss']
})
export class ClientListLayoutComponent {

  @ViewChild(ClientTableComponent) child: ClientTableComponent;

  constructor() {
  }

  updateTable(client: ClientListDto): void {
    this.child.updateTable(client);
  }
}
