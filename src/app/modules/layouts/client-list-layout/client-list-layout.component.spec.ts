import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientListLayoutComponent } from './client-list-layout.component';

describe('ClientListLayoutComponent', () => {
  let component: ClientListLayoutComponent;
  let fixture: ComponentFixture<ClientListLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientListLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientListLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
