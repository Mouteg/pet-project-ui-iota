import { Component, ViewChild } from '@angular/core';
import { CarTableComponent } from '../../components/car/car-table/car-table.component';
import { CarListDto } from '../../../models/car.model';

@Component({
  selector: 'app-car-list-layout',
  templateUrl: './car-list-layout.component.html',
  styleUrls: ['./car-list-layout.component.scss']
})
export class CarListLayoutComponent {

  @ViewChild(CarTableComponent) child: CarTableComponent;

  constructor() {
  }

  updateTable(car: CarListDto): void {
    this.child.updateTable(car);
  }
}
