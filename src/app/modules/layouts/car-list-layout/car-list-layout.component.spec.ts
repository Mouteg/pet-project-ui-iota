import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarListLayoutComponent } from './car-list-layout.component';

describe('CarListLayoutComponent', () => {
  let component: CarListLayoutComponent;
  let fixture: ComponentFixture<CarListLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarListLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarListLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
