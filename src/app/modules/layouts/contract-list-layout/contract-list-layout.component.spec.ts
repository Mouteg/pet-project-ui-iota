import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractListLayoutComponent } from './contract-list-layout.component';

describe('ContractListLayoutComponent', () => {
  let component: ContractListLayoutComponent;
  let fixture: ComponentFixture<ContractListLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractListLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractListLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
