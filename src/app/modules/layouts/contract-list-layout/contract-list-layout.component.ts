import { Component, ViewChild } from '@angular/core';
import { InsuranceContractListDto } from '../../../models/insurance-contract.model';
import { ContractTableComponent } from '../../components/contract/contract-table/contract-table.component';

@Component({
  selector: 'app-contract-list-layout',
  templateUrl: './contract-list-layout.component.html',
  styleUrls: ['./contract-list-layout.component.scss']
})
export class ContractListLayoutComponent {

  @ViewChild(ContractTableComponent) child: ContractTableComponent;

  constructor() {
  }

  updateTable(contract: InsuranceContractListDto): void {
    this.child.updateTable(contract);
  }
}
