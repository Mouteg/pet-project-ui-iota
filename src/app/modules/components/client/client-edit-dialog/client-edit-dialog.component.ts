import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ClientEditDto } from '../../../../models/client.model';
import { FormControl, FormGroup } from '@angular/forms';
import { CustomValidators } from '../../../../validators/custom-validators';
import { ClientService } from '../../../../service/client.service';

@Component({
  selector: 'app-client-edit-dialog',
  templateUrl: './client-edit-dialog.component.html',
  styleUrls: ['./client-edit-dialog.component.scss']
})
export class ClientEditDialogComponent implements OnInit {
  birthDate: FormControl;
  firstName: FormControl;
  lastName: FormControl;
  clientEditFormGroup: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public client: ClientEditDto,
    private service: ClientService,
    private dialogRef: MatDialogRef<ClientEditDialogComponent>
  ) {
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.firstName = new FormControl(this.client.firstName);
    this.lastName = new FormControl(this.client.lastName);
    this.birthDate = new FormControl(this.client.birthDate, [CustomValidators.past]);

    this.clientEditFormGroup = new FormGroup({
      birthDate: this.birthDate,
      firstName: this.firstName,
      lastName: this.lastName
    });
  }

  submitForm(): void {
    if (this.hasChanged()) {
      this.updateClient();
      this.service.update(this.client).subscribe(client => {
        this.dialogRef.close(client);
      });
    }
  }

  private updateClient(): void {
    this.client.firstName = this.firstName.value;
    this.client.lastName = this.lastName.value;
    this.client.birthDate = this.birthDate.value;
  }

  private hasChanged(): boolean {
    return !(
      this.client.firstName === this.firstName.value &&
      this.client.lastName === this.lastName.value &&
      this.client.birthDate === this.birthDate.value);
  }
}
