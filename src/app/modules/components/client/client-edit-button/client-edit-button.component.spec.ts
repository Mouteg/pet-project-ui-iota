import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientEditButtonComponent } from './client-edit-button.component';

describe('ClientEditButtonComponent', () => {
  let component: ClientEditButtonComponent;
  let fixture: ComponentFixture<ClientEditButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientEditButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientEditButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
