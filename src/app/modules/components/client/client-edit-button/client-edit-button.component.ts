import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ClientEditDialogComponent } from '../client-edit-dialog/client-edit-dialog.component';
import { ClientService } from '../../../../service/client.service';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-client-edit-button',
  templateUrl: './client-edit-button.component.html',
  styleUrls: ['./client-edit-button.component.scss']
})
export class ClientEditButtonComponent {

  @Input() id: number;
  @Output() tableUpdated: EventEmitter<any> = new EventEmitter();

  constructor(
    private service: ClientService,
    private dialog: MatDialog
  ) {
  }

  openClientEditDialog(): void {
    this.service.getById(this.id).subscribe(response => {
      this.dialog.open(ClientEditDialogComponent, {
        data: response
      }).afterClosed().subscribe(client => {
        this.tableUpdated.emit(client);
      });
    });
  }

}
