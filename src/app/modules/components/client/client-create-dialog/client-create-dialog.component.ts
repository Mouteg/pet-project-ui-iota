import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ClientService } from '../../../../service/client.service';
import { CustomValidators } from '../../../../validators/custom-validators';
import { MatDialogRef } from '@angular/material/dialog';
import { ClientCreateDto } from '../../../../models/client.model';

@Component({
  selector: 'app-client-create-dialog',
  templateUrl: './client-create-dialog.component.html',
  styleUrls: ['./client-create-dialog.component.scss']
})
export class ClientCreateDialogComponent implements OnInit {
  minPasswordLength = 4;

  email: FormControl;
  password: FormControl;
  birthDate: FormControl;
  firstName: FormControl;
  lastName: FormControl;

  clientCreateFormGroup: FormGroup;

  constructor(private service: ClientService,
              private dialogRef: MatDialogRef<ClientCreateDialogComponent>) {
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.firstName = new FormControl(this.firstName);
    this.lastName = new FormControl(this.lastName);
    this.password = new FormControl(this.password, [
      Validators.required,
      Validators.minLength(this.minPasswordLength)]);
    this.email = new FormControl(this.email, [
      Validators.email,
      Validators.required]);
    this.birthDate = new FormControl(this.birthDate, [CustomValidators.past]);

    this.clientCreateFormGroup = new FormGroup({
      email: this.email,
      password: this.password,
      birthDate: this.birthDate,
      firstName: this.firstName,
      lastName: this.lastName
    });

    this.email.valueChanges.subscribe(value => this.checkEmailAvailability(value));
  }

  submitForm(): void {
    this.service.create(this.buildCreateRequest()).subscribe(
      (client) => {
        this.dialogRef.close(client);
      }
    );
  }

  private buildCreateRequest(): ClientCreateDto {
    return {
      email: this.email.value,
      password: this.password.value,
      birthDate: this.birthDate.value,
      firstName: this.firstName.value,
      lastName: this.lastName.value
    };
  }

  checkEmailAvailability(email: string): void {
    this.service.checkEmailAvailability(email).subscribe(response => {
      if (!response) {
        this.email.setErrors({ notUnique: true });
      }
    });
  }
}
