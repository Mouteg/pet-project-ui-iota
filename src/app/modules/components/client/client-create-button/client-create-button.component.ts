import { Component, EventEmitter, Output } from '@angular/core';
import { ClientCreateDialogComponent } from '../client-create-dialog/client-create-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-client-create-button',
  templateUrl: './client-create-button.component.html',
  styleUrls: ['./client-create-button.component.scss']
})
export class ClientCreateButtonComponent {

  @Output() tableUpdated: EventEmitter<any> = new EventEmitter();

  constructor(private dialog: MatDialog) {
  }

  openClientCreateDialog(): void {
    this.dialog.open(ClientCreateDialogComponent).afterClosed().subscribe(client => {
      this.tableUpdated.emit(client);
    });
  }
}
