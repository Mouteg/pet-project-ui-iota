import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientCreateButtonComponent } from './client-create-button.component';

describe('ClientCreateButtonComponent', () => {
  let component: ClientCreateButtonComponent;
  let fixture: ComponentFixture<ClientCreateButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientCreateButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientCreateButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
