import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientInfoButtonComponent } from './client-info-button.component';

describe('ClientInfoButtonComponent', () => {
  let component: ClientInfoButtonComponent;
  let fixture: ComponentFixture<ClientInfoButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientInfoButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientInfoButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
