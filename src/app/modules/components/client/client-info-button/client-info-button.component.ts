import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ClientInfoDialogComponent } from '../client-info-dialog/client-info-dialog.component';
import { ClientService } from '../../../../service/client.service';

@Component({
  selector: 'app-client-info-button',
  templateUrl: './client-info-button.component.html',
  styleUrls: ['./client-info-button.component.scss']
})
export class ClientInfoButtonComponent{

  @Input() id: number;

  constructor(
    private service: ClientService,
    private dialog: MatDialog) {
  }

  openClientCreateDialog(): void {
    this.service.getById(this.id).subscribe(response => {
      this.dialog.open(ClientInfoDialogComponent, {
        data: response
      });
    });
  }
}
