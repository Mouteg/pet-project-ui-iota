import { Component, Injectable, OnInit, ViewChild } from '@angular/core';
import { ClientService } from '../../../../service/client.service';
import { MatTableDataSource } from '@angular/material/table';
import { ClientListDto } from 'src/app/models/client.model';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-client-table',
  templateUrl: './client-table.component.html',
  styleUrls: ['./client-table.component.scss']
})
@Injectable()
export class ClientTableComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private service: ClientService) {
  }
  paginatorOptions = [30, 20, 10, 5];
  columnsToDisplay = ['id', 'firstName', 'lastName', 'birthDate', 'email', 'info', 'edit'];
  clients: Array<ClientListDto> = [];
  dataSource = new MatTableDataSource<ClientListDto>();

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.loadTable();
  }

  loadTable(): void {
    this.service.getAll().subscribe(response => {
      this.clients = response;
      this.dataSource.data = this.clients;
    });
  }

  updateTable(client: ClientListDto): void {
    if (client) {
      const index = this.clients.findIndex(existedClient => {
        return existedClient.id === client.id;
      });
      if (index !== -1) {
        this.clients[index] = client;
      } else {
        this.clients.push(client);
      }
      this.dataSource.data = this.clients;
    }
  }
}
