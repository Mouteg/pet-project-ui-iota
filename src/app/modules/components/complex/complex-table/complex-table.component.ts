import { Component, OnInit, ViewChild } from '@angular/core';
import { ComplexService } from '../../../../service/complex.service';
import { MatTableDataSource } from '@angular/material/table';
import { InsuranceComplex } from '../../../../models/insurance-complex.model';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-complex-table',
  templateUrl: './complex-table.component.html',
  styleUrls: ['./complex-table.component.scss']
})
export class ComplexTableComponent implements OnInit {
  columnsToDisplay = ['id', 'duration', 'compensationPercent', 'damageLevel', 'coveredPart'];
  dataSource = new MatTableDataSource<InsuranceComplex>();

  constructor(private service: ComplexService) {
  }

  ngOnInit(): void {
    this.loadTable();
  }

  loadTable(): void {
    this.service.getAll().subscribe(complexes => {
      this.dataSource.data = complexes;
    });
  }

}
