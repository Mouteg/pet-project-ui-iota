import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ContractService } from '../../../../service/contract.service';
import { InsuranceContractListDto } from '../../../../models/insurance-contract.model';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-contract-table',
  templateUrl: './contract-table.component.html',
  styleUrls: ['./contract-table.component.scss']
})
export class ContractTableComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  columnsToDisplay = ['id', 'clientId', 'carId', 'complexIds'];
  dataSource = new MatTableDataSource();
  contracts: Array<InsuranceContractListDto>;
  paginatorOptions = [30, 20, 10, 5];
  constructor(private service: ContractService) {
  }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.loadTable();
  }

  loadTable(): void {
    this.service.getAll().subscribe(response => {
      this.contracts = response;
      this.dataSource.data = this.contracts;
    });
  }

  updateTable(contract: InsuranceContractListDto): void {
    if (contract) {
      this.contracts.push(contract);
      this.dataSource.data = this.contracts;
    }
  }

}
