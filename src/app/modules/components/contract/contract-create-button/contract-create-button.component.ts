import { Component, EventEmitter, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ContractCreateDialogComponent } from '../contract-create-dialog/contract-create-dialog.component';

@Component({
  selector: 'app-contract-create-button',
  templateUrl: './contract-create-button.component.html',
  styleUrls: ['./contract-create-button.component.scss']
})
export class ContractCreateButtonComponent {

  @Output() tableUpdated: EventEmitter<any> = new EventEmitter();

  constructor(private dialog: MatDialog) {
  }

  openContractCreateDialog(): void {
    this.dialog.open(ContractCreateDialogComponent).afterClosed().subscribe(contract => {
      this.tableUpdated.emit(contract);
    });
  }

}
