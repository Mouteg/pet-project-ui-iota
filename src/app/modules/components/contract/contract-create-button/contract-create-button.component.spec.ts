import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractCreateButtonComponent } from './contract-create-button.component';

describe('ContractCreateButtonComponent', () => {
  let component: ContractCreateButtonComponent;
  let fixture: ComponentFixture<ContractCreateButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractCreateButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractCreateButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
