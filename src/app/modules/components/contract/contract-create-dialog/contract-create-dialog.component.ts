import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ContractService } from '../../../../service/contract.service';
import { MatDialogRef } from '@angular/material/dialog';
import { InsuranceContractCreateDto } from '../../../../models/insurance-contract.model';
import { ClientService } from '../../../../service/client.service';
import { CarService } from '../../../../service/car.service';
import { ComplexService } from '../../../../service/complex.service';

@Component({
  selector: 'app-contract-create-dialog',
  templateUrl: './contract-create-dialog.component.html',
  styleUrls: ['./contract-create-dialog.component.scss']
})
export class ContractCreateDialogComponent implements OnInit {

  clientId: FormControl;
  carId: FormControl;
  complexesIds: FormControl;

  contractCreateFormGroup: FormGroup;

  clientLookups;
  carLookups;
  complexesLookups;

  constructor(private contractService: ContractService,
              private clientService: ClientService,
              private carService: CarService,
              private complexService: ComplexService,
              private dialogRef: MatDialogRef<ContractCreateDialogComponent>) {
  }

  ngOnInit(): void {
    this.loadLookups();
    this.initForm();
  }

  initForm(): void {
    this.clientId = new FormControl(this.clientId, [Validators.required]);
    this.carId = new FormControl(this.carId, [Validators.required]);
    this.complexesIds = new FormControl(this.complexesIds, [Validators.required]);

    this.contractCreateFormGroup = new FormGroup({
      clientId: this.clientId,
      carId: this.carId,
      complexesIds: this.complexesIds
    });
  }

  submitForm(): void {
    this.contractService.create(this.buildCreateRequest()).subscribe(
      (contract) => {
        this.dialogRef.close(contract);
      });
  }

  loadLookups(): void {
    this.clientService.getLookup().subscribe(response => {
        this.clientLookups = response;
      }
    );
    this.carService.getLookup().subscribe(response => {
        this.carLookups = response;
      }
    );
    this.complexService.getLookup().subscribe(response => {
        this.complexesLookups = response;
      }
    );
  }

  buildCreateRequest(): InsuranceContractCreateDto {
    return {
      clientId: this.clientId.value,
      carId: this.carId.value,
      insuranceComplexesIds: this.complexesIds.value
    };
  }
}
