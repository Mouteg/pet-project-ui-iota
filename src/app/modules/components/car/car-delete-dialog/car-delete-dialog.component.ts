import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CarService } from '../../../../service/car.service';

@Component({
  selector: 'app-car-delete-dialog',
  templateUrl: './car-delete-dialog.component.html',
  styleUrls: ['./car-delete-dialog.component.scss']
})
export class CarDeleteDialogComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public id: number,
              private dialogRef: MatDialogRef<CarDeleteDialogComponent>,
              private service: CarService) {
  }

  closeDialog(): void {
    this.dialogRef.close(false);
  }

  deleteCar(): void {
    this.service.delete(this.id);
    this.dialogRef.close(true);
  }

}
