import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Car } from '../../../../models/car.model';

@Component({
  selector: 'app-car-info-dialog',
  templateUrl: './car-info-dialog.component.html',
  styleUrls: ['./car-info-dialog.component.scss']
})
export class CarInfoDialogComponent{

  constructor(@Inject(MAT_DIALOG_DATA) public car: Car) {
  }
}
