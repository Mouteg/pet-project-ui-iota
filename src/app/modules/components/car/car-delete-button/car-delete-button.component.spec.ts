import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarDeleteButtonComponent } from './car-delete-button.component';

describe('CarDeleteButtonComponent', () => {
  let component: CarDeleteButtonComponent;
  let fixture: ComponentFixture<CarDeleteButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarDeleteButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarDeleteButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
