import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CarDeleteDialogComponent } from '../car-delete-dialog/car-delete-dialog.component';

@Component({
  selector: 'app-car-delete-button',
  templateUrl: './car-delete-button.component.html',
  styleUrls: ['./car-delete-button.component.scss']
})
export class CarDeleteButtonComponent {

  @Input() id: number;
  @Output() itemDeleted: EventEmitter<any> = new EventEmitter<any>();

  constructor(private dialog: MatDialog) {
  }

  openCarDeleteDialog(): void {
    this.dialog.open(CarDeleteDialogComponent, {
      data: this.id
    }).afterClosed().subscribe(deleted => {
      if (deleted) {
        this.itemDeleted.emit(this.id);
      }
    });
  }
}
