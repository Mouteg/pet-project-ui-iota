import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CarInfoDialogComponent } from '../car-info-dialog/car-info-dialog.component';
import { CarService } from '../../../../service/car.service';

@Component({
  selector: 'app-car-info-button',
  templateUrl: './car-info-button.component.html',
  styleUrls: ['./car-info-button.component.scss']
})
export class CarInfoButtonComponent {

  @Input() id: number;

  constructor(
    private service: CarService,
    private dialog: MatDialog) {
  }

  openCarCreateDialog(): void {
    this.service.getById(this.id).subscribe(response => {
      this.dialog.open(CarInfoDialogComponent, {
        data: response
      });
    });
  }
}
