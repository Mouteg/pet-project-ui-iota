import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarInfoButtonComponent } from './car-info-button.component';

describe('CarInfoButtonComponent', () => {
  let component: CarInfoButtonComponent;
  let fixture: ComponentFixture<CarInfoButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarInfoButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarInfoButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
