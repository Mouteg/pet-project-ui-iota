import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from '../../../../validators/custom-validators';
import { CarCreateDto, Color } from '../../../../models/car.model';
import { CarService } from '../../../../service/car.service';
import { ClientService } from '../../../../service/client.service';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-car-create-dialog',
  templateUrl: './car-create-dialog.component.html',
  styleUrls: ['./car-create-dialog.component.scss']
})
export class CarCreateDialogComponent implements OnInit {

  clientId: FormControl;
  color: FormControl;
  engineCapacity: FormControl;
  yearOfManufacture: FormControl;
  weight: FormControl;

  carCreateFormGroup: FormGroup;

  colors: Color[] = Object.keys(Color).map(k => Color[k as any]);
  clientLookups;

  constructor(private carService: CarService,
              private clientService: ClientService,
              private dialogRef: MatDialogRef<CarCreateDialogComponent>) {
  }

  ngOnInit(): void {
    this.initForm();
    this.loadLookups();
  }

  initForm(): void {
    this.color = new FormControl(this.color, [Validators.required]);
    this.engineCapacity = new FormControl(this.engineCapacity, [Validators.required, Validators.min(0), CustomValidators.digit]);
    this.yearOfManufacture = new FormControl(this.yearOfManufacture, [Validators.required, CustomValidators.past]);
    this.weight = new FormControl(this.weight, [Validators.required, Validators.min(0), CustomValidators.digit]);
    this.clientId = new FormControl(this.clientId, [Validators.required, Validators.min(0)]);

    this.carCreateFormGroup = new FormGroup({
      color: this.color,
      engineCapacity: this.engineCapacity,
      yearOfManufacture: this.yearOfManufacture,
      weight: this.weight,
      clientId: this.clientId
    });
  }

  loadLookups(): void {
    this.clientService.getLookup().subscribe(response => {
        this.clientLookups = response;
      }
    );
  }

  submitForm(): void {
    this.carService.create(this.buildCreateRequest()).subscribe(car => {
        this.dialogRef.close(car);
      });
  }

  private buildCreateRequest(): CarCreateDto {
    return {
      color: this.color.value,
      weight: this.weight.value,
      yearOfManufacture: this.yearOfManufacture.value,
      engineCapacity: this.engineCapacity.value,
      clientId: this.clientId.value
    };
  }
}
