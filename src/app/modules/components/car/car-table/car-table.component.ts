import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { CarListDto } from '../../../../models/car.model';
import { CarService } from '../../../../service/car.service';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-car-table',
  templateUrl: './car-table.component.html',
  styleUrls: ['./car-table.component.scss']
})
export class CarTableComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  columnsToDisplay = ['id', 'color', 'engineCapacity', 'weight', 'yearOfManufacture', 'info', 'delete'];
  paginatorOptions = [30, 20, 10, 5];
  cars: Array<CarListDto> = [];
  dataSource = new MatTableDataSource<CarListDto>();

  constructor(private service: CarService) {
  }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.loadTable();
  }

  loadTable(): void {
    this.service.getAll().subscribe(response => {
      this.cars = response;
      this.dataSource.data = this.cars;
    });
  }

  updateTable(car: CarListDto): void {
    if (car) {
      this.cars.push(car);
      this.dataSource.data = this.cars;
    }
  }

  deleteCar(id: number): void {
    const index = this.cars.findIndex(car => car.id === id);
    this.cars.splice(index, 1);
    this.dataSource.data = this.cars;
  }
}
