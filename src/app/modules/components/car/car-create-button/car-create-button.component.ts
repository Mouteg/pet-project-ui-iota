import { Component, EventEmitter, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CarCreateDialogComponent } from '../car-create-dialog/car-create-dialog.component';

@Component({
  selector: 'app-car-create-button',
  templateUrl: './car-create-button.component.html',
  styleUrls: ['./car-create-button.component.scss']
})
export class CarCreateButtonComponent{

  @Output() tableUpdated: EventEmitter<any> = new EventEmitter();


  constructor(private dialog: MatDialog) {
  }

  openCarCreateDialog(): void {
    this.dialog.open(CarCreateDialogComponent).afterClosed().subscribe(car => {
      this.tableUpdated.emit(car);
    });
  }
}
