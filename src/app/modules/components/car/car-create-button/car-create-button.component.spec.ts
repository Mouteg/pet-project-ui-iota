import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarCreateButtonComponent } from './car-create-button.component';

describe('CarCreateButtonComponent', () => {
  let component: CarCreateButtonComponent;
  let fixture: ComponentFixture<CarCreateButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarCreateButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarCreateButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
